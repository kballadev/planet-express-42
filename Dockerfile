FROM python:3.9-slim

ENV RABBITMQ_HOST planet-express-charts-rabbitmq

ADD requirements.txt /worker/requirements.txt
RUN pip install -r /worker/requirements.txt
ADD src /worker

# RUN python -m spacy download es_core_news_lg
RUN python -m spacy download en_core_web_lg
