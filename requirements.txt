coverage==5.3
pika==1.1.0
pytest==6.1.2
spacy==2.3.4
python-dateutil==2.8.1
