from cleaners.dateformatter import DateFormatter
from cleaners.tokenizer import Tokenized


class Cleaner():
    def __init__(self, body_json):
        self.content = body_json.get('content')
        self.title = body_json.get('title')
        self.date_times = body_json.get('date_time')
        self.autors = body_json.get('autor')
        self.data = {
            'url': body_json.get('url'),
            'autors': self.autors.split(";") if self.autors else [],
            'title': self.title
        }

    def clean(self):
        self.clean_content()
        self.clean_date_times()

    def clean_content(self):
        if self.content:
            tokenized = Tokenized(self.content)
            tokenized_list = tokenized.clean_text()['entities']
            if self.title:
                tokenized_title_list = Tokenized(self.title).clean_text()['entities']
                for keyword in tokenized_title_list:
                    index_coincidence = next((i for i, item in enumerate(tokenized_list) if item["word"] == keyword['word']), None)
                    if index_coincidence != None:
                        tokenized_list[index_coincidence]['score'] = tokenized_list[index_coincidence]['score'] + keyword['score']/2
                    else:
                        tokenized_list.append(keyword)
            self.data['tokenized'] = [{'entities': tokenized_list}]
        else:
            self.data['tokenized'] = []
    
    def clean_date_times(self):
        if self.date_times:
            date_formater = DateFormatter(self.date_times, self.data['url'])
            self.data['date_times'] = date_formater.string_to_timestamp()
        else:
            self.data['date_times'] = []
