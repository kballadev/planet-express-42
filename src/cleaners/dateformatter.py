import logging

from dateutil.parser import parse

from settings import LOG_LEVEL

logger = logging.getLogger(__name__)
logging.basicConfig(level=LOG_LEVEL)


class DateFormatter():
    def __init__(self, datetimes, url):
        self.datetimes = datetimes.split(";")
        self.url = url

    def string_to_timestamp(self):
        timestamps = []
        
        for date_time in self.datetimes:
            try:
                timestamp = parse(date_time).timestamp()
                timestamps.append(timestamp)
            except Exception as e:
                logger.warning("Can't parser datetime %s from %s", date_time, self.url)
        
        return timestamps


