import os

RABBITMQ_HOST = os.getenv("RABBITMQ_HOST", "localhost")
RABBITMQ_PORT = os.getenv("RABBITMQ_PORT", 5672)
RABBITMQ_USER = os.getenv("RABBITMQ_USER", "user")
RABBITMQ_PASSWORD = os.getenv("RABBITMQ_PASSWORD", "eHFkTW5NdzZiSQ==")

SPACY_LANG = os.getenv("SPACY_LANG", "en")
spacy_words_to_avoid = os.getenv("SPACY_WORDS_TO_AVOID", 'abstract')
SPACY_WORDS_TO_AVOID = spacy_words_to_avoid.split(',') if spacy_words_to_avoid else spacy_words_to_avoid


LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')