import json

from cleaners.cleaner import Cleaner


class TestCleaner:
    def test_tokenized_item(self):
        with open('tests/input.json', 'r') as input_json:
            input_data = json.load(input_json)
            cleaner_instance = Cleaner(input_data)
            cleaner_instance.clean()
            tokenized_list = cleaner_instance.data['tokenized'][0]['entities']

        with open('tests/output.json', 'r') as output_json:
            output_data = json.load(output_json)
            data_tokenized_list = output_data['tokenized'][0]['entities']
            assert len(tokenized_list) == len(data_tokenized_list)
            assert all([a == b for a, b in zip(tokenized_list,
                        data_tokenized_list)])
            assert all([a == b for a, b in zip(cleaner_instance.data['date_times'],
                        output_data['date_times'])])