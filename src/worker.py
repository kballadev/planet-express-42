#!/usr/bin/env python
import argparse
import logging
import threading

from rabbitmq.async_consumer import Consumer
from rabbitmq.async_publisher import Publisher
from settings import LOG_LEVEL

logger = logging.getLogger(__name__)
logging.basicConfig(level=LOG_LEVEL)


def init_worker(queue, queue_out, text_type, pdf_separator):
    publisher = Publisher(queue_out)
    thread_publisher = threading.Thread(target=publisher.run)
    thread_publisher.start()

    consumer = Consumer(queue, text_type, pdf_separator, publisher)
    thread_consumer = threading.Thread(target=consumer.run)
    thread_consumer.start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Init worker')
    parser.add_argument('queue', help='Queue to read messages')
    parser.add_argument('queue_out', help='Queue to write messages')
    parser.add_argument('--text-type', help='Type of text to tokenize',
                        default='text')
    parser.add_argument('--pdf_separator', help='PDF paragraph text separator',
                        default='  \x0c')
    args = parser.parse_args()
    init_worker(args.queue, args.queue_out, args.text_type, args.pdf_separator)
